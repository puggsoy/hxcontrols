package hx.controls;

class Palettes
{
	static public var current(default, null):Array<Array<UInt>> = DEFAULT();
	static public var overGrey(default, null):UInt = 0x77FFFFFF;
	
	static inline public function DEFAULT():Array<Array<UInt>>
	{
		return [[0xFFACACAC, 0xFFF0F0F0],
				[0xFF858585, 0xFFD2D2D2],
				[0xFF5E5E5E, 0xFFA4A4A4]];
	}
}
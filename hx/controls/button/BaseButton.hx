package hx.controls.button;
import openfl.display.Bitmap;
import openfl.display.BitmapData;
import openfl.events.Event;
import openfl.events.MouseEvent;
import openfl.geom.Rectangle;
import openfl.display.Sprite;
import openfl.ui.Mouse;
import hx.controls.State;
import haxe.Log;

/**
 * Base button class. Handles core stuff like clicking aesthetics and events.
 */
class BaseButton extends Sprite
{
	private var _img:Bitmap;
	private var _states:Map<State, BitmapData>;
	private var _overGrey:Bitmap;
	
	/**
	 * Whether the button is enabled or not.
	 */
	public var enabled(default, set):Bool = true;
	
	/**
	 * Function to call when the button is clicked.
	 */
	public var onClick(default, set):MouseEvent -> Void;
	
	/**
	 * Constructor.
	 * @param	width Width of the button.
	 * @param	height Height of the button.
	 */
	public function new(width:Int, height:Int)
	{
		super();
		
		mouseChildren = false;
		
		var innerRect:Rectangle = new Rectangle(1, 1, width - 2, height - 2);
		
		var temp:BitmapData = new BitmapData(width, height, true, Palettes.current[0][0]);
		temp.fillRect(innerRect, Palettes.current[0][1]);
		
		_states = new Map<State, BitmapData>();
		_states.set(Up, temp);
		
		temp = new BitmapData(width, height, true, Palettes.current[1][0]);
		temp.fillRect(innerRect, Palettes.current[1][1]);
		
		_states.set(Over, temp);
		
		temp = new BitmapData(width, height, true, Palettes.current[2][0]);
		temp.fillRect(innerRect, Palettes.current[2][1]);
		
		_states.set(Down, temp);
		
		_img = new Bitmap(_states[Up]);
		addChild(_img);
		
		temp = new BitmapData(width, height, true, Palettes.overGrey);
		_overGrey = new Bitmap(temp);
		
		addEventListener(MouseEvent.MOUSE_OVER, changeState);
		addEventListener(MouseEvent.MOUSE_OUT, changeState);
		addEventListener(MouseEvent.MOUSE_DOWN, changeState);
		addEventListener(MouseEvent.MOUSE_UP, changeState);
		//addEventListener(MouseEvent.CLICK, onClick);
	}
	
	private function changeState(e:MouseEvent)
	{
		switch(e.type)
		{
			case MouseEvent.MOUSE_OVER:
				_img.bitmapData = _states[Over];
			case MouseEvent.MOUSE_OUT:
				_img.bitmapData = _states[Up];
			case MouseEvent.MOUSE_DOWN:
				_img.bitmapData = _states[Down];
			case MouseEvent.MOUSE_UP:
				_img.bitmapData = _states[Over];
		}
	}
	
	private function set_enabled(v:Bool):Bool
	{
		if (v)
		{
			if (!enabled)
			{
				addEventListener(MouseEvent.MOUSE_OVER, changeState);
				addEventListener(MouseEvent.MOUSE_OUT, changeState);
				addEventListener(MouseEvent.MOUSE_DOWN, changeState);
				addEventListener(MouseEvent.MOUSE_UP, changeState);
				
				if (onClick != null)
				{
					addEventListener(MouseEvent.CLICK, onClick);
				}
				
				removeChild(_overGrey);
			}
		}
		else
		{
			_img.bitmapData = _states[Up];
			
			if (enabled)
			{
				removeEventListener(MouseEvent.MOUSE_OVER, changeState);
				removeEventListener(MouseEvent.MOUSE_OUT, changeState);
				removeEventListener(MouseEvent.MOUSE_DOWN, changeState);
				removeEventListener(MouseEvent.MOUSE_UP, changeState);
				
				if (onClick != null)
				{
					removeEventListener(MouseEvent.CLICK, onClick);
				}
				
				addChild(_overGrey);
			}
		}
		
		enabled = v;
		
		return enabled;
	}
	
	private function set_onClick(f:MouseEvent -> Void):MouseEvent -> Void
	{
		if (hasEventListener(MouseEvent.CLICK))
		{
			removeEventListener(MouseEvent.CLICK, onClick);
		}
		
		onClick = f;
		
		if (onClick != null && enabled)
		{
			addEventListener(MouseEvent.CLICK, onClick);
		}
		
		return onClick;
	}
}
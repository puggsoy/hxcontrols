package hx.controls.button;
import openfl.display.BitmapData;
import openfl.display.Bitmap;

/**
 * A button with an icon or picture on it.
 */
class IconButton extends BaseButton
{
	private var _icon:Bitmap;
	
	/**
	 * Constructor.
	 * @param	width Width of the button.
	 * @param	height Height of the button.
	 * @param	icon BitmapData of the icon to go on the button.
	 */
	public function new(width:Int, height:Int, icon:BitmapData) 
	{
		super(width, height);
		
		_icon = new Bitmap(icon);
		_icon.x = Std.int((width / 2) - (_icon.width / 2));
		_icon.y = Std.int((height / 2) - (_icon.height / 2));
		
		addChild(_icon);
	}
	
	/**
	 * Flips the image's icon.
	 * @param	vertical Whether the image should be flipped vertically (true) or horizontally (false).
	 */
	public function flipIcon(vertical:Bool = false)
	{
		if (vertical)
		{
			_icon.scaleY = -1;
			_icon.y += _icon.height;
		}
		else
		{
			_icon.scaleX = -1;
			_icon.x += _icon.width;
		}
	}
}
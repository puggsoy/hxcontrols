package hx.controls.button;
import openfl.text.TextField;
import openfl.text.TextFieldAutoSize;
import openfl.text.TextFieldType;
import openfl.text.TextFormat;
import openfl.text.TextFormatAlign;
import haxe.Log;

/**
 * A button with text on it.
 */
class LabelButton extends BaseButton
{
	/**
	 * Constructor.
	 * @param	width Width of the button.
	 * @param	height Height of the button.
	 * @param	label Text to display on the button.
	 */
	public function new(width:Int, height:Int, label:String) 
	{
		super(width, height);
		
		var txtFormat:TextFormat = new TextFormat("Arial", 12, 0xFF000000);
		txtFormat.align = TextFormatAlign.CENTER;
		
		var txt:TextField = new TextField();
		txt.defaultTextFormat = txtFormat;
		txt.width = width;
		txt.height = 0;
		txt.type = TextFieldType.DYNAMIC;
		txt.selectable = false;
		txt.wordWrap = true;
		txt.text = label;
		txt.autoSize = TextFieldAutoSize.LEFT;
		
		if (txt.height > height)
		{
			trace("foo");
			txt.autoSize = TextFieldAutoSize.NONE;
			txt.height = height;
		}
		
		txt.x = (width / 2) - (txt.width / 2);
		txt.y = (height / 2) - (txt.height / 2);
		
		addChild(txt);
	}
}
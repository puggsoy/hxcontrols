package hx.controls.input;
import hx.controls.button.IconButton;
import openfl.display.BitmapData;
import openfl.display.Sprite;
import openfl.events.Event;
import openfl.events.MouseEvent;
import openfl.events.TextEvent;
import openfl.text.TextField;
import openfl.text.TextFieldType;
import openfl.text.TextFormat;
import openfl.text.TextFormatAlign;
import haxe.Log;
import openfl.Assets;
import openfl.display.Bitmap;

/**
 * A numerical stepper.
 */
class NumericStepper extends Sprite
{
	private var _numField:TextField;
	private var _upStep:IconButton;
	private var _downStep:IconButton;
	private var _overGrey:Bitmap;
	
	/**
	 * Whether the stepper is enabled or not.
	 */
	public var enabled(default, set):Bool = true;
	
	/**
	 * Value of the stepper. If this is set beyond <code>min</code> or <code>max</code> it will be clamped to fit.
	 */
	public var value(default, set):Int;
	
	/**
	 * The minimum value allowed. If this is set higher than <code>value</code> then <code>value</code> will be set to this value. 0 by default.
	 */
	public var min(default, set):Int = 0;
	
	/**
	 * The maximum value allowed. If this is set lower than <code>value</code> then <code>value</code> will be set to this value. 100 by default.
	 */
	public var max(default, set):Int = 100;
	
	/**
	 * Constructor.
	 * @param	initValue The initial value of the stepper.
	 */
	public function new(?initValue:Int = 0) 
	{
		super();
		
		var txtFormat:TextFormat = new TextFormat("Arial", 12, 0xFF000000);
		txtFormat.align = TextFormatAlign.LEFT;
		
		_numField = new TextField();
		_numField.defaultTextFormat = txtFormat;
		_numField.borderColor = 0xFF212121;
		_numField.width = 35;
		_numField.height = 19;
		_numField.border = true;
		_numField.type = TextFieldType.DYNAMIC;
		_numField.selectable = false;
		addChild(_numField);
		
		var arrDat:BitmapData = Assets.getBitmapData("ui/vertArr.png");
		
		_upStep = new IconButton(20, 10, arrDat);
		_upStep.x = _numField.width;
		_upStep.y = _numField.y;
		_upStep.onClick = function(e:MouseEvent) { value++; };
		addChild(_upStep);
		
		_downStep = new IconButton(20, 10, arrDat);
		_downStep.flipIcon(true);
		_downStep.x = _upStep.x;
		_downStep.y = _upStep.height;
		_downStep.onClick = function(e:MouseEvent) { value--; };
		addChild(_downStep);
		
		var temp:BitmapData = new BitmapData(Std.int(_numField.width), Std.int(_numField.height), true, Palettes.overGrey);
		_overGrey = new Bitmap(temp);
		
		value = initValue;
	}
	
	public function set_value(v:Int):Int
	{
		if (v > max) v = max;
		if (v < min) v = min;
		
		value = v;
		
		_numField.text = '$value';
		
		
		return value;
	}
	
	public function set_min(v:Int):Int
	{
		min = v;
		set_value(value);
		
		return min;
	}
	
	public function set_max(v:Int):Int
	{
		max = v;
		set_value(value);
		
		return max;
	}
	
	private function set_enabled(v:Bool):Bool
	{
		if (v)
		{
			if (!enabled)
			{
				_upStep.enabled = true;
				_downStep.enabled = true;
				
				removeChild(_overGrey);
			}
		}
		else
		{
			if (enabled)
			{
				_upStep.enabled = false;
				_downStep.enabled = false;
				
				addChild(_overGrey);
			}
		}
		
		enabled = v;
		
		return enabled;
	}
}
package hx.controls.scroll;
import flash.geom.Matrix3D;
import openfl.display.BitmapData;
import haxe.Log;
import hx.controls.button.IconButton;
import hx.controls.Palettes;
import hx.controls.scroll.ScrollBarDirection;
import hx.controls.scroll.ScrollPolicy;
import openfl.Assets;
import openfl.display.Bitmap;
import openfl.display.Sprite;
import openfl.events.Event;
import openfl.events.MouseEvent;
import hx.controls.State;

class ScrollBar extends Sprite
{
	public static inline var POSITION_CHANGE:String = "positionChange";
	
	private var _track:Sprite;
	private var _btn1:IconButton;
	private var _btn2:IconButton;
	private var _thumb:ScrollThumb;
	
	/**
	 * Length of the entire scroll bar (including buttons). This is the height for a vertical scroll bar, width for a horizontal scroll bar.
	 */
	public var length:Int;
	
	/**
	 * Whether the scroll bar is horizontal or vertical.
	 */
	public var direction(default, null):ScrollBarDirection;
	
	/**
	 * The state policy of the scroll bar. A value of <code>ScrollPolicy.On</code> indicates that the scroll bar is always on; a value of <code>ScrollPolicy.Off</code> indicates that the scroll bar is always off; a value of <code>ScrollPolicy.Auto</code> indicates that the scroll bar's state automatically changes.
	 */
	public var scrollPolicy:ScrollPolicy = Auto;
	
	/**
	 * The minimum position of the scroll bar.
	 */
	public var minPosition(default, null):Int = 0;
	
	/**
	 * The maximum position of the scroll bar.
	 */
	public var maxPosition(default, null):Int = 100;
	
	/**
	 * The size of the view that the scroll bar is scrolling for. It is essentially the height or width of the area that the scrolled content can be viewed within.
	 */
	public var viewSize(default, set):Int;
	
	/**
	 * The number of pixels that the buttons scroll by.
	 */
	public var btnIncrement:Int = 1;
	
	/**
	 * The size of the content being scrolled through.
	 */
	public var contentSize(default, set):Int;
	
	/**
	 * Whether the scroll bar is enabled or disabled. True if <code>viewSize < contentSize</code>, otherwise false.
	 */
	public var enabled(default, null):Bool = true;
	
	private var _trackLength:Int = 0;
	private var _position:Int = 0;
	
	/**
	 * The position the scroll bar is at. Must be between <code>maximum</code> and <code>minimum</code> inclusive.
	 */
	public var position(get, set):Int;
	private var initialized:Bool = false;
	
	/**
	 * Constuctor.
	 * @param	width How thick the scroll bar is.
	 * @param	length How long the scroll bar is. <code>viewSize</code> will initially be equal to this.
	 * @param	direction Whether the scroll bar is horizontal or vertical.
	 * @param	contentSize The size of the content being scrolled through.
	 */
	public function new(thickness:Int, length:Float, direction:ScrollBarDirection, contentSize:Float)
	{
		super();
		
		this.length = Std.int(length);
		this.direction = direction;
		this.contentSize = Std.int(contentSize);
		
		viewSize = this.length;
		maxPosition = this.contentSize - viewSize;
		
		if (direction == Horizontal)
		{
			var arrDat:BitmapData = Assets.getBitmapData("ui/horiArr.png");
			
			_btn1 = new IconButton(15, thickness, arrDat);
			
			_btn2 = new IconButton(15, thickness, arrDat);
			_btn2.flipIcon();
		}
		else
		{
			var arrDat:BitmapData = Assets.getBitmapData("ui/vertArr.png");
			
			_btn1 = new IconButton(thickness, 15, arrDat);
			
			_btn2 = new IconButton(thickness, 15, arrDat);
			_btn2.flipIcon(true);
		}
		
		_btn1.onClick = function(e) { position = position - btnIncrement; };
		_btn2.onClick = function(e) { position = position + btnIncrement; };
		
		makeTrack();
		
		if (contentSize <= viewSize) disable();
		else makeThumb();
		
		initialized = true;
	}
	
	private function makeTrack()
	{
		if (_track != null )
		{
			removeChild(_track);
		}
		
		var w:Int;
		var h:Int;
		
		_trackLength = length - 30;
		
		if (direction == Horizontal)
		{
			w = _trackLength;
			h = Std.int(_btn1.height);
		}
		else
		{
			w = Std.int(_btn1.width);
			h = _trackLength;
		}
		
		var trackDat:BitmapData = new BitmapData(w, h, true, Palettes.current[0][1]);
		_track = new Sprite();
		_track.addChild(new Bitmap(trackDat));
		addChild(_track);
		
		_track.addEventListener(MouseEvent.CLICK, trackMove);
		
		removeChild(_btn1);
		removeChild(_btn2);
		
		if (direction == Horizontal)
		{
			_btn2.x = length - _btn2.width;
			
			_track.x += _btn1.width;
		}
		else
		{
			_btn2.y = length - _btn2.height;
			
			_track.y += _btn1.height;
		}
		
		addChild(_btn1);
		addChild(_btn2);
	}
	
	private function makeThumb()
	{
		var viewFraction:Float = viewSize / contentSize;
		
		var thumbWidth:Int = 0;
		var thumbLength:Int = Std.int(_trackLength * viewFraction);
		
		if (thumbLength < 15)
		{
			thumbLength = 15;
		}
		
		var thumbX:Int = 0;
		var thumbY:Int = 0;
		
		if (direction == Horizontal)
		{
			thumbWidth = Std.int(_btn1.height);
			thumbX = Std.int(_btn1.width);
		}
		else
		{
			thumbWidth = Std.int(_btn1.width);
			thumbY = Std.int(_btn1.height);
		}
		
		_thumb = new ScrollThumb(thumbWidth, thumbLength, direction);
		_thumb.x = thumbX;
		_thumb.y = thumbY;
		_thumb.addEventListener("thumbMove", onThumbMove);
		addChild(_thumb);
		
		position = _position;
	}
	
	private function onThumbMove(?e:Event)
	{
		if(direction == Horizontal) position = Std.int((maxPosition * ((_thumb.x - _track.x) / (_trackLength - _thumb.length))));
		else position = Std.int((maxPosition * ((_thumb.y - _track.y) / (_trackLength - _thumb.length))));
	}
	
	private function checkThumbBounds()
	{
		switch(direction)
		{
			case Horizontal:
				if (_thumb.x < _btn1.width) _thumb.x = _btn1.width;
				if (_thumb.x > (_btn2.x - _thumb.width)) _thumb.x = (_btn2.x - _thumb.width);
			case Vertical:
				if (_thumb.y < _btn1.height) _thumb.y = _btn1.height;
				else
				if (_thumb.y > (_btn2.y - _thumb.height)) _thumb.y = (_btn2.y - _thumb.height);
		}
	}
	
	private function trackMove(e:MouseEvent)
	{
		var behind:Bool;
		
		if (direction == Horizontal)
		{
			if (e.localX < _thumb.x) behind = true;
			else behind = false;
		}
		else
		{
			if (e.localY < _thumb.y) behind = true;
			else behind = false;
		}
		
		if (behind)
		{
			position -= viewSize;
		}
		else
		{
			position += viewSize;
		}
	}
	
	private function enable()
	{
		if (scrollPolicy == Off) return;
		
		enabled = true;
		
		_btn1.enabled = true;
		_btn2.enabled = true;
		makeThumb();
		
		if (!_track.hasEventListener(MouseEvent.CLICK))
		{
			_track.addEventListener(MouseEvent.CLICK, trackMove);
		}
	}
	
	private function disable()
	{
		if (scrollPolicy == On) return;
		
		enabled = false;
		
		_btn1.enabled = false;
		_btn2.enabled = false;
		
		removeChild(_thumb);
		if (_thumb != null && _thumb.hasEventListener("thumbMove"))
		{
			_thumb.removeEventListener("thumbMove", onThumbMove);
		}
		
		if (_track.hasEventListener(MouseEvent.CLICK))
		{
			_track.removeEventListener(MouseEvent.CLICK, trackMove);
		}
	}
	
	public function get_position():Int
	{
		return _position;
	}
	
	public function set_position(value:Int):Int 
	{
		_position = value;
		
		if (_position > maxPosition)
		{
			_position = maxPosition;
		}
		else
		if (_position < minPosition)
		{
			_position = minPosition;
		}
		
		var trackPos:Float = ((_trackLength - _thumb.length) * (_position / maxPosition));
		
		if (direction == Horizontal) _thumb.x = Math.round(_track.x + trackPos);
		else _thumb.y = Math.round(_track.y + trackPos);
		
		checkThumbBounds();
		
		dispatchEvent(new Event(POSITION_CHANGE));
		
		return _position;
	}
	
	public function set_viewSize(v:Int):Int
	{
		viewSize = v;
		
		if (initialized)
		{
			if (enabled)
			{
				if (viewSize < contentSize)
				{
					enable();
				}
				else
				{
					disable();
				}
			}
			else
			{
				if (viewSize < contentSize)
				{
					enable();
				}
			}
		}
		
		return viewSize;
	}
	
	public function set_length(v:Int):Int
	{
		length = v;
		
		if (enabled && initialized)
		{
			makeTrack();
			makeThumb();
		}
		
		return length;
	}
	
	public function set_contentSize(v:Int):Int
	{
		contentSize = v;
		
		if (initialized)
		{
			if (enabled)
			{
				if (viewSize < contentSize)
				{
					enable();
				}
				else
				{
					disable();
				}
			}
			else
			{
				if (viewSize < contentSize)
				{
					enable();
				}
			}
		}
		
		return contentSize;
	}
}
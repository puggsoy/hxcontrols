package hx.controls.scroll;

enum ScrollBarDirection 
{
	Horizontal;
	Vertical;
}
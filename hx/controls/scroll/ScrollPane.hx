package hx.controls.scroll;
import openfl.display.Sprite;
import hx.controls.scroll.ScrollPolicy;

class ScrollPane extends Sprite
{
	private var horiScroll:ScrollBar;
	private var vertiScroll:ScrollBar;
	
	/**
	 * The content of the scrollpane. You can either set this, or add children to it directly.
	 */
	public var content:Sprite;
	
	/**
	 * The state policy of the horizontal scroll bar.
	 */
	public var horiPolicy:ScrollPolicy = Auto;
	
	/**
	 * The state policy of the vertical scroll bar.
	 */
	public var vertPolicy:ScrollPolicy = Auto;
	
	public function new() 
	{
		
	}
}
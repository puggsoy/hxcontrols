package hx.controls.scroll;
import openfl.display.BitmapData;
import openfl.display.Bitmap;
import openfl.display.Sprite;
import hx.controls.State;
import openfl.events.Event;
import openfl.events.MouseEvent;
import hx.controls.scroll.ScrollBarDirection;
import openfl.geom.Point;
import openfl.Lib;

/**
 * Utility scroll thumb class (mostly to handle drawing easily).
 */
class ScrollThumb extends Sprite
{
	private var _img:Bitmap;
	private var _states:Map<State, BitmapData>;
	private var _scrollDir:ScrollBarDirection;
	private var _lastPos:Point;
	private var _dragPoint:Point;
	
	public var length:Int;
	public var dragging(default, null):Bool;
	
	/**
	 * Constructor.
	 * @param	width Width of the thumb.
	 * @param	height Height of the thumb.
	 * @param   scrollDir Whether the thumb scrolls horizontally or vertically.
	 */
	public function new(width:Int, length:Int, scrollDir:ScrollBarDirection)
	{
		super();
		
		this.length = length;
		_scrollDir = scrollDir;
		dragging = false;
		
		_states = new Map<State, BitmapData>();
		
		var w:Int;
		var h:Int;
		
		if (_scrollDir == Horizontal)
		{
			w = length;
			h = width;
		}
		else
		{
			w = width;
			h = length;
		}
		
		var temp:BitmapData = new BitmapData(w, h, true, Palettes.current[0][0]);
		_states.set(Up, temp);
		
		temp = new BitmapData(w, h, true, Palettes.current[1][0]);
		_states.set(Over, temp);
		
		temp = new BitmapData(w, h, true, Palettes.current[2][0]);
		_states.set(Down, temp);
		
		_img = new Bitmap(_states[Up]);
		addChild(_img);
		
		addEventListener(MouseEvent.MOUSE_OVER, changeState);
		addEventListener(MouseEvent.MOUSE_OUT, changeState);
		addEventListener(MouseEvent.MOUSE_DOWN, changeState);
	}
	
	private function changeState(e:MouseEvent)
	{
		if (dragging)
		{
			if (e.type != MouseEvent.MOUSE_UP)
			{
				return;
			}
		}
		
		switch(e.type)
		{
			case MouseEvent.MOUSE_OVER:
				_img.bitmapData = _states[Over];
			case MouseEvent.MOUSE_OUT:
				_img.bitmapData = _states[Up];
			case MouseEvent.MOUSE_DOWN:
				_lastPos = new Point(x, y);
				_dragPoint = new Point(e.stageX, e.stageY);
				_img.bitmapData = _states[Down];
				dragging = true;
				Lib.current.stage.addEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
		}
	}
	
	private function onMouseMove(e:MouseEvent)
	{
		if (!e.buttonDown)
		{
			endDrag(e);
			return;
		}
		
		if (_scrollDir == Horizontal) x = _lastPos.x + (e.stageX - _dragPoint.x);
		else y = _lastPos.y + (e.stageY - _dragPoint.y);
		
		dispatchEvent(new Event("thumbMove"));
	}
	
	private function endDrag(e:MouseEvent)
	{
		if (mouseX > 0 && mouseX < width && mouseY > 0 && mouseY < height)
		{
			_img.bitmapData = _states[Over];
		}
		else
		{
			_img.bitmapData = _states[Up];
		}
		
		
		dragging = false;
		Lib.current.stage.removeEventListener(MouseEvent.MOUSE_MOVE, onMouseMove);
	}
}